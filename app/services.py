from create_tables import user, atm_records, conn


def insert_user_tid(telegram_id):
    insert = user.insert()
    conn.execute(insert, {"telegram_id": telegram_id})


def insert_event_values(data):
    insert = atm_records.insert()
    for d in data:
        conn.execute(insert, {"address": d["address"], "dollar_amount": d["amount"]})


def get_users_tid():
    select = user.select()
    cursor = conn.execute(select)
    return cursor.fetchall()


def delete_user(telegram_id):
    delete = user.delete()
    conn.execute(delete.where(user.c.telegram_id == telegram_id))
