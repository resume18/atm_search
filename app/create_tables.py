import datetime

from sqlalchemy import MetaData, Column, Integer, create_engine, String, DateTime, Table
import os
from dotenv import load_dotenv

load_dotenv()

DB_USER = os.environ.get("DB_USER")
DB_PASSWORD = os.environ.get("DB_PASSWORD")
DB_HOST = os.environ.get("DB_HOST")
DB_PORT = os.environ.get("DB_PORT")
DB_NAME = os.environ.get("DB_NAME")

engine = create_engine(f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}", echo=True)
meta = MetaData()
conn = engine.connect()

user = Table(
    "users_tg_ids", meta,
    Column("telegram_id", Integer, primary_key=True)
)


atm_records = Table(
    "atm_has_cash_records", meta,
    Column("id", Integer, primary_key=True),
    Column("address", String),
    Column("amount", Integer),
    Column("time", DateTime, default=datetime.datetime.now())
)

meta.create_all(engine)
