from aiogram import executor
from aiogram.types import Message
from services import insert_user_tid, delete_user
from aiogram import Dispatcher, Bot
from dotenv import load_dotenv
import os

load_dotenv()

TOKEN = os.environ.get("TOKEN")

bot = Bot(TOKEN)
dp = Dispatcher(bot)


@dp.message_handler(commands=["start"])
async def start(message: Message):
    insert_user_tid(message.from_user.id)
    await message.answer("Вы подписались на рассылку.")


@dp.message_handler(commands=["stop"])
async def stop(message: Message):
    delete_user(message.from_user.id)
    await message.answer("Вы отписались от рассылки.")

if __name__ == "__main__":
    executor.start_polling(dp)
