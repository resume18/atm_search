from aiogram.utils.exceptions import ChatNotFound
from telegram_bot import bot
from services import get_users_tid
from celery import Celery
import os
from dotenv import load_dotenv

load_dotenv()

CELERY_BROKER_URL = os.environ.get("CELERY_BROKER_URL", 'redis://localhost:6379/0')

app = Celery("app", broker=CELERY_BROKER_URL)
app.conf.update(CELERY_TASKS_ALWAYS_EAGER=True)
app.autodiscover_tasks()


async def send_message(points):
    users = get_users_tid()
    answer_message = ''
    for p in points:
        answer_message += f"Адрес {p['address']}. Сумма {p['amount']}$.\n"
    users = get_users_tid()
    for u in users:
        try:
            await bot.send_message(u[0], answer_message)
        except ChatNotFound:
            pass


# @app.task(queue="default")
# async def send_message_in_bot_task(points):
#     asyncio.run(send_message(points))
