import json
from typing import List
from fastapi import WebSocket, WebSocketDisconnect, FastAPI
from services import insert_event_values
from starlette.responses import HTMLResponse
from fastapi_utils.tasks import repeat_every
from tasks import send_message
import aiohttp

app = FastAPI()

html = """
<!DOCTYPE html>
<html>
    <head>
        <title>Chat</title>
    </head>
    <body>
        <ul id='list'>
        </ul>
        <div id='time'>
        <div>
        <script>
            var client_id = Date.now()
            var ws = new WebSocket(`ws://localhost:8000/ws/${client_id}`);
            ws.onmessage = function(event) {
                var list = document.getElementById('list')
                var time = document.getElementById('time')
                time.innerHTML = Date().toLocaleString()
                while (list.firstChild) {
                    list.removeChild(list.firstChild);
                }
                var received_json = JSON.parse(event.data)
                received_json.forEach(function(item, i, received_json) {
                    var content = document.createTextNode(`Адрес: ${item.address}. Сумма: ${item.amount}`)
                    var record = document.createElement('li')
                    record.appendChild(content)
                    list.appendChild(record)
                });
            };
        </script>
    </body>
</html>
"""


class ConnectionManager:
    def __init__(self):
        self.active_connections: List[WebSocket] = []

    async def connect(self, websocket: WebSocket):
        await websocket.accept()
        self.active_connections.append(websocket)

    def disconnect(self, websocket: WebSocket):
        self.active_connections.remove(websocket)

    async def send_personal_message(self, message: str, websocket: WebSocket):
        await websocket.send_text(message)

    async def broadcast(self, message: str):
        for connection in self.active_connections:
            await connection.send_text(message)


manager = ConnectionManager()


@app.on_event("startup")
@repeat_every(seconds=10)
async def poll():
    payload = {"bounds": {"bottomLeft": {"lat": 55.70819116602968, "lng": 37.64013405834954},
                            "topRight": {"lat": 55.777912649666014, "lng": 37.66313668286126}},
               "filters": {"banks": ["tcs"], "showUnavailable": True, "currencies": ["USD"]}, "zoom": 13}
    url = 'https://api.tinkoff.ru/geo/withdraw/clusters'
    async with aiohttp.ClientSession() as session:
        async with session.post(url, json=payload) as response:
            result = await response.json()
            if result:
                records = []
                for cluster in result['payload']['clusters']:
                    record = {}
                    for point in cluster['points']:
                        record["address"] = point["address"]
                        record["amount"] = point["limits"][0]["amount"]
                    records.append(record)
                insert_event_values(records)
            else:
                records = [{"address": 'Нет нужных банкоматов', "amount": "0"}]
            await manager.broadcast(json.dumps(records))
            await send_message(records)


@app.get("/")
async def get():
    return HTMLResponse(html)


@app.websocket("/ws/{client_id}")
async def websocket_endpoint(websocket: WebSocket, client_id: int):
    await manager.connect(websocket)
    try:
        while True:
            await websocket.receive_text()
    except WebSocketDisconnect:
        manager.disconnect(websocket)
